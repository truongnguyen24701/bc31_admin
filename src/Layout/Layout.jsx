import React from 'react'
import HeaderTheme from '../Components/HeaderTheme/HeaderTheme'

export default function Layout({ children }) {
    return (
        <div>
            <HeaderTheme />
            {children}
        </div>
    )
}
