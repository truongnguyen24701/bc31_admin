
import { createSlice } from '@reduxjs/toolkit';
import { data } from 'autoprefixer';
const initialState = { user: null }

const userSlice = createSlice({
    name: "userSlice",
    initialState,
    reducers: {
        setUserInfor: (state, { payload }) => {
            state.user = payload
        }
    }
})

export const { setUserInfor } = userSlice.actions

export default userSlice.reducer