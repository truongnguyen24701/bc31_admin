import LoginPage from "../Page/LoginPage/LoginPage";
import NotFoundPage from "../Page/NotFoundPage/NotFoundPage";
import UserManagementPage from "../Page/UserManagementPage/UserManagementPage";

export const adminRoute = [
    {
        path: "/",
        Component: UserManagementPage,
    },
    {
        path: "/user-management",
        Component: UserManagementPage,
    },
    {
        path: "/login",
        Component: LoginPage,
    },
    {
        path: "*",
        Component: NotFoundPage,
    },
]