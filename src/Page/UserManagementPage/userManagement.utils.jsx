import { Space, Table, Tag } from 'antd';


export const columns = [
    {
        title: 'Tài Khoản',
        dataIndex: 'taiKhoan',
        key: 'taiKhoan',
    },
    {
        title: 'Họ Tên',
        dataIndex: 'hoTen',
        key: 'hoTen',
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
    },
    {
        title: 'Số điện thoại',
        dataIndex: 'soDT',
        key: 'soDT',
    },
    {
        title: 'Mật Khẩu',
        dataIndex: 'matKhau',
        key: 'matKhau',
    },
    {
        title: 'Loại người dùng',
        dataIndex: 'maLoaiNguoiDung',
        key: 'maLoaiNguoiDung',
        render: (text) => {
            if (text == "QuanTri") {
                return <Tag color={'red'}> Quản trị </Tag>
            } else {
                return <Tag color={'blue'}>Khách hàng</Tag>
            }
        }
    },
    {
        title: 'Thao tác',
        dataIndex: 'action',
        key: 'action',
    },

];