import React from 'react'
import { userService } from './../../../services/user.service';
import { message } from 'antd';

export default function UserAction({ taiKhoan, onSuccess }) {

    let handleUserDelete = () => {
        userService
            .deleteUser(taiKhoan)
            .then((res) => {
                message.success("Xoá thành công")
                console.log(res);
                onSuccess() // khi xoá thành công tự xoá kh phải load lại trang web
            })
            .catch((err) => {
                message.error(err.response.data.content)
                console.log(err);
            })
    }

    return (
        <div className='space-x-5'>
            <button className='bg-blue-500 rounded text-white px-5 py-3'>Sửa</button>

            <button
                onClick={handleUserDelete}
                className='bg-red-500 rounded text-white px-5 py-3'>Xoá</button>
        </div>
    )
}
