import { Space, Table, Tag } from 'antd';
import React, { useEffect, useState } from 'react';
import { columns } from './../userManagement.utils';
import { movieService } from './../../../services/movie.service';
import UserAction from './UserAction';


const UserTable = () => {
    let [dataUser, setDataUser] = useState([])
    useEffect(() => {
        let fetchListUser = () => {  // khi xoá thành công tự xoá kh phải load lại trang web
            movieService
                .getUserList()
                .then((res) => {
                    let userList = res.content.map((user) => {
                        return {
                            ...user, action: <UserAction
                                onSuccess={fetchListUser} // khi xoá thành công tự xoá kh phải load lại trang web
                                taiKhoan={user.taiKhoan} />
                        }
                    })
                    setDataUser(userList)
                    console.log(res);
                })
                .catch((err) => {
                    console.log(err);
                })
        }
        fetchListUser() // khi xoá thành công tự xoá kh phải load lại trang web
    }, []);
    return < Table columns={columns} dataSource={dataUser} />
};

export default UserTable;

