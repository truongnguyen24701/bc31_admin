import React, { Component } from 'react'
import UserTable from './UserTable/UserTable'

export default class UserManagementPage extends Component {
  render() {
    return (
      <div className='container m-auto py-20'>
        <UserTable />
      </div>
    )
  }
}
