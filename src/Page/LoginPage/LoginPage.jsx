import { Form, Input, message } from 'antd';
import React from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom'
import { setUserInfor } from '../../redux/slice/userSlice';
import { userService } from './../../services/user.service';
import { localStorageServ } from './../../services/localStorageService';


export default function LoginPage() {
    let dispatch = useDispatch()
    let history = useNavigate()
    const onFinish = (values) => {
        console.log("values", values);

        userService.postLogin(values)
            .then((res) => {
                // lưu lên redux
                dispatch(setUserInfor(res.content))
                // lưu vào localStorage
                localStorageServ.user.set(res.content)
                message.success("Đăng nhập thành công")
                console.log(res);
                setTimeout(() => {
                    history("/")
                }, 1000)
            })
            .catch((err) => {
                message.error(err.response.data.content)
                console.log(err);
            })

    };


    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className='bg-red-400 h-screen w-screen p-10'>
            <div className='container mx-auto bg-white p-10 rounded-xl  flex'>
                <div className='w-full'>
                    <Form
                        name="basic"
                        layout='vertical'
                        labelCol={{
                            span: 24,
                        }}
                        wrapperCol={{
                            span: 24,
                        }}
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Tài khoản</p>}
                            name="taiKhoan"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tài khoản!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label={<p className='font-medium text-blue-700'>Mật khẩu</p>}
                            name="matKhau"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mật khẩu!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <div className='flex justify-center'>
                            <button className='rounded px-5 py-2 text-white bg-blue-500'>Đăng nhập</button>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    )
}

