import React from 'react'
import { DesktopResponsive, MobileResponsive, TabletResponsive } from './../../HOC/ResponsiveComponent';
import Header_Desktop from './Header_Desktop';
import Header_Mobie from './Header_Mobie';
import Header_Tablet from './Header_Tablet';

export default function HeaderTheme() {
    return (
        <>
            <DesktopResponsive>
                <Header_Desktop />
            </DesktopResponsive>

            <TabletResponsive>
                <Header_Tablet />
            </TabletResponsive>

            <MobileResponsive>
                <Header_Mobie />
            </MobileResponsive>
        </>
    )
}
