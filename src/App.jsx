import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import LoginPage from './Page/LoginPage/LoginPage';
import UserManagementPage from './Page/UserManagementPage/UserManagementPage';
import NotFoundPage from './Page/NotFoundPage/NotFoundPage';
import { adminRoute } from './routes/adminRoute';
import Layout from './Layout/Layout';

function App() {
  let renderRoutes = () => {
    return adminRoute.map(({ path, Component }) => {
      return <Route path={path} element={<Layout>
        <Component />
      </Layout>} />
    })
  }
  return (
    <div className="App">
      <BrowserRouter>
        {/* <Routes>
          <Route path='/login' element={<LoginPage />} />
          <Route path={'/'} element={<UserManagementPage />} />
          <Route path={'/user-management'} element={<UserManagementPage />} />
          <Route path='*' element={<NotFoundPage />} />
        </Routes> */}
        <Routes>
          {renderRoutes()}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
